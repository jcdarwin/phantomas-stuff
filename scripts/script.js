;
var extra = (function() {
    var closeButton  = document.getElementById( 'p--modal__close' );

    // use onkeypress for keys other than 'esc'
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            closeButton.click();
        }
    };

})();
