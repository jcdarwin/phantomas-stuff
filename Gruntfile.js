module.exports = function(grunt) {

    "use strict";

    var config = grunt.file.readJSON('config.json');

    grunt.initConfig( {

        // Metadata.
        pkg: grunt.file.readJSON('package.json'),
        deploy: grunt.file.exists('deploy.json') && grunt.file.readJSON('deploy.json'),

        phantomas: {
            section : {
              options : {
                indexPath                 : config.section.root,
                url                       : config.section.url,
                numberOfRuns              : 6,
                //'allow-domain'          : 'cdn.yoursite.com.br,ajax.googleapis.com',
                'no-externals'            : true,
                'timeout'                 : 30,
                options: {
                    'timeout'             : 300,
                    //'proxy'               : '<%= deploy.proxy %>',
                    //'proxyauth'           : '<%= deploy.proxyauth %>',
                    //'progress'            : true,
                    //'debug'               : 'phantomas',
                    //'verbose'             : true,
                    //'log'                 : '/usr/share/nginx/www/phantomas.stuff.co.nz/phantomas.log',
                },
                // Assertions can be changed from one run to another
                assertions : {
                    // REQUESTS
                    'notFound'              : 0,
                    'assetsWithQueryString' : 3,
                    'biggestResponse'       : 500,
                    'biggestLatency'        : 2000,
                    // TIMINGS
                    'timeToFirstByte'       : 500,
                    'commentsSize'          : 0,
                    'consoleMessages'       : 0,
                    'windowAlerts'          : 0,
                    'windowConfirms'        : 0,
                    'windowPrompts'         : 0,
                    'jsErrors'              : 0,
                    'gzipRequests'          : 8,
                    'nodesWithInlineCSS'    : 0,
                    'requests'              : 100,
                    'DOMelementsCount'      : 500,
                },
                // We include `group` here for documentation purposes.
                // We use `group` to specify just the panels that we want
                // in the results, but if we don't specify `group`, we'll
                // still get all the panels by default.
                group : {
                  'REQUESTS' : [
                    'requests',
                    'gzipRequests',
                    //'postRequests',
                    //'httpsRequests',
                    'notFound',
                    //'multipleRequests',
                    'maxRequestsPerDomain',
                    'domains',
                    'medianRequestsPerDomain',
                    'redirects',
                    'redirectsTime',
                    //'smallestResponse',
                    'biggestResponse',
                    'smallestLatency',
                    'biggestLatency',
                    'medianResponse',
                    'medianLatency',
                    //'assetsNotGzipped',
                    //'assetsWithQueryString',
                    'smallImages'
                  ],
                  'TIMINGS' : [
                    'timeToFirstByte',
                    'timeToLastByte',
                    'timeToFirstCss',
                    'timeToFirstJs',
                    //'timeToFirstImage',
                    'fastestResponse',
                    'slowestResponse',
                    'onDOMReadyTime',
                    'onDOMReadyTimeEnd',
                    'windowOnLoadTime',
                    'windowOnLoadTimeEnd',
                    'httpTrafficCompleted',
                    //'timeBackend',
                    //'timeFrontend'
                  ],
                  'HTML' : [
                    'bodyHTMLSize',
                    'iframesCount',
                    'imagesWithoutDimensions',
                    'commentsSize',
                    'hiddenContentSize',
                    'whiteSpacesSize',
                    'DOMelementsCount',
                    'DOMelementMaxDepth',
                    'nodesWithInlineCSS',
                    'foo'
                  ],
                  'JAVASCRIPT' : [
                    //'eventsBound',
                    //'documentWriteCalls',
                    //'evalCalls',
                    'jsErrors',
                    'consoleMessages',
                    'windowAlerts',
                    'windowConfirms',
                    'windowPrompts',
                    'globalVariables',
                    'localStorageEntries',
                    'ajaxRequests'
                  ],
                  'DOM' : [
                    'DOMqueries',
                    'DOMqueriesById',
                    'DOMqueriesByClassName',
                    'DOMqueriesByTagName',
                    'DOMqueriesByQuerySelectorAll',
                    'DOMinserts',
                    'DOMqueriesDuplicated'
                  ],
/*
                  'HEADERS' : [
                    'headersCount',
                    'headersSentCount',
                    'headersRecvCount',
                    'headersSize',
                    'headersSentSize',
                    'headersRecvSize'
                  ],
*/
                  'CACHING' : [
                    'cacheHits',
                    'cacheMisses',
                    'cachePasses',
                    'cachingNotSpecified',
                    'cachingTooShort',
                    'cachingDisabled'
                  ],
                  'COOKIES' : [
                    'cookiesSent',
                    'cookiesRecv',
                    'domainsWithCookies',
                    'documentCookiesLength',
                    'documentCookiesCount'
                  ],
                  'COUNTS & SIZES' : [
                    'contentLength',
                    'bodySize',
                    'htmlSize',
                    'htmlCount',
                    'cssSize',
                    'cssCount',
                    'jsSize',
                    'jsCount',
                    'jsonSize',
                    'jsonCount',
                    'imageSize',
                    'imageCount',
                    'webfontSize',
                    'webfontCount',
                    'base64Size',
                    'base64Count',
                    //'otherCount',
                    //'otherSize'
                  ],
                  'JQUERY' : [
                    'jQueryOnDOMReadyFunctions',
                    'jQuerySizzleCalls'
                  ]
                }
              }
            }
        },

        copy: {
            scripts: {
                files: [
                    {expand: true, src: ['scripts/**'], dest: config.section.public},
                ]
            }
        },

        sass: {
            section: {
              options: {
                style: 'expanded',
                compass: true
              },
              files: [{
                expand: true,
                // cwd: config.section.sassdir,
                cwd: './sass',
                src: ['phantomas.scss'],
                //dest: config.section.css
                dest: config.section.sassdir,
                ext: '.css'
              }]
            }
        },


        dom_munger: {
            section: {
              options: {
                append: {selector:'body',html:'<script src="public/scripts/script.js"></script>'},
              },
              src: config.section.root + '/index.html', //could be an array of files
              dest: config.section.root + '/index.html' //optional, if not specified the src file will be overwritten
            },
        },

        'http-server': {

            section: {

                // the server root directory
                root: config.section.root,

                port: 8000,
                // port: function() { return 8282; }

                host: "127.0.0.1",

                // cache: <sec>,
                showDir : true,
                autoIndex: true,

                // server default file extension
                ext: "html",

                // run in parallel with other tasks
                runInBackground: false
            }

        },

        prompt: {
            deploy_json: {
                options: {
                    questions: [
                    {
                      config: 'prompt.deploy_json',
                      type: 'confirm', // list, checkbox, confirm, input, password
                      message: 'Are you SURE you\'ve got the right server settings in deploy.json (CTRL-C if not)?',
                      default: false, // default value if nothing is entered
                      validate: function(value) {
                        return value == 'Yes' || 'OK: hit CTRL-C';
                      }, // return true if valid, error message if invalid
                    }
                  ]
                }
            },
        },

        shell: {
          // We shell out to compile our sass as we need to use rvm to load a recent ruby.
          // Note that we source multiple rvm scripts -- one will be found on my local,
          // while the other should exist on the remote.
          sass:{
            command: [
              'bash -c "source $HOME/.rvm/scripts/rvm"',
              'bash -c "source /usr/local/rvm/scripts/rvm',
              'sass sass/phantomas.scss ' + config.section.sassdir + config.section.css + 'phantomas.css --style=expanded --compass'
            ].join(';'),
            options: {
              stdout: true,
              stderr: true,
              failOnError: false
            }
          },
          zip: {
            command: [
              'rm -rf <%= pkg.name %>.zip',
              'zip -rDX9 <%= pkg.name %>.zip ./* -x "*node_modules*" *.DS_Store deploy.json .git .gitignore dist'
            ].join('&&'),
            options: {
              stdout: true,
              stderr: true,
              failOnError: false
            }
          },
        },
        sftp: {
          deploy: {
            files: {
              "./": "<%= pkg.name %>.zip"
            },
            options: {
              path: '/tmp/',
              host: '<%= deploy.host %>',
              username: '<%= deploy.username %>',
              password: '<%= deploy.password %>',
              srcBasePath: './',
              showProgress: true
            }
          }
        },
        sshexec: {
            deploy_clean: {
                command: [
                  'rm -rf /tmp/<%= pkg.name %>.zip',
                  'sudo rm -rf <%= deploy.root %><%= pkg.name %>.old',
                  'sudo mv <%= deploy.root %><%= pkg.name %> <%= deploy.root %><%= pkg.name %>.old',
                ].join(';'),
                options: {
                  host: '<%= deploy.host %>',
                  username: '<%= deploy.username %>',
                  password: '<%= deploy.password %>',
                  ignoreErrors: true
                }
            },
            deploy: {
                // unzip the current package into place and install
                command: [
                  'sudo unzip /tmp/<%= pkg.name %>.zip -d <%= deploy.root %><%= pkg.name %>',
                  'sudo chown -R www-data:wheel <%= deploy.root %><%= pkg.name %>',
                  'cd <%= deploy.root %><%= pkg.name %>',
                  'npm install',
                ].join('&&'),
                options: {
                  host: '<%= deploy.host %>',
                  username: '<%= deploy.username %>',
                  password: '<%= deploy.password %>'
                }
            }
        }

    } );

    grunt.loadNpmTasks('grunt-phantomas');
    grunt.loadNpmTasks('grunt-http-server');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-dom-munger');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-ssh');
    grunt.loadNpmTasks('grunt-shell');
    grunt.loadNpmTasks('grunt-prompt');

    // The default task runs phantomas on the site described in config.json,
    // and then serves the result at http://localhost:8000/
    grunt.registerTask('default', ['section', 'http-server:section']);

    // The section task runs phantomas on the site described in config.json.
    grunt.registerTask('section', ['phantomas:section', 'copy:scripts', 'shell:sass', 'dom_munger:section']);

    // The deploy tasks zips and moves the package into place on the environment being deployed to,
    // and sets permissions.
    // You'll need a deploy.json (refer to the README.md)
    grunt.registerTask('deploy', ['prompt:deploy_json', 'shell:zip', 'sshexec:deploy_clean', 'sftp:deploy', 'sshexec:deploy']);

};
